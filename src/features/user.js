import { createSlice } from '@reduxjs/toolkit'

export const userSlice = createSlice({
    name: 'user',
    initialState: {
        isLogged: false,
        isColored: false,
        email: '',
        password: ''
    },
    reducers: {
        login: (state, action) => {
            state.isLogged = true;
            state.email = action.payload.email;
            state.password = action.payload.password;
        },
        logout: (state, action) => {
            state.isLogged = false;
            state.email = "";
            state.password = "";
        },
        coloring: (state, action) => {
            state.isColored = true;
            
        }
    }
})
export const { login, logout,coloring } = userSlice.actions;
export default userSlice.reducer;