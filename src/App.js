import './App.css';
import Login from './login';
import Profile from './Profile';
import Color from './color';
import {store} from './store';
import {Provider} from 'react-redux';
function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Profile/>
        <Login/>
        <Color/>
      </Provider>

    </div>
  );
}

export default App;
