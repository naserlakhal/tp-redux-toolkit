import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
// useDispatch is a hook that allows us to dispatch actions
import { login, logout } from './features/user'
export default function Login() {
    const dispatch = useDispatch();
    const state = useSelector((state) => state.user)
    const stateColor = useSelector((state) => state.color)
    console.log('checking the state', state)
    return (
        <div>
            {stateColor.isColored ? <h1 style={{color: "red"}}>Login</h1> : <h1>Login</h1>}
            {state.isLogged ? (<button
                onClick={() => dispatch(logout())}
            > se déconnecter </button>) : (<button
                onClick={() => dispatch(login({ email: 'nasri@codeup.com', password: 'aftercode', isLogged: true }))}
            > se connecter </button>)}

        </div>
    )
}