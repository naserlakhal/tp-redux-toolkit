import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
// useDispatch is a hook that allows us to dispatch actions
import { colorRed , colorinitial} from './features/color'
export default function Color() {
    const dispatch = useDispatch();
    const state = useSelector((state) => state.color)
    return (
        <div>
            <h1>Color</h1>
            {state.isColored ? (<button 
                onClick={() => dispatch(colorinitial())}
            >inital color</button>) :(<button 
                onClick={() => dispatch(colorRed())}
            >red color</button>) }
            
            

        </div>
    )
}