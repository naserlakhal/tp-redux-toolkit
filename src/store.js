import {configureStore} from '@reduxjs/toolkit'
import userReducer from './features/user'
import ColorReducer from './features/color'

export const store = configureStore({
    reducer: {
        color: ColorReducer,
        user: userReducer
    }
})